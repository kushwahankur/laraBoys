import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Test11Component } from './test11/test11.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [Test11Component]
})
export class MyModule1Module { }
