import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AbcComponent } from './abc/abc.component';

const abc: Routes = [
  { path: '',  component: AbcComponent }
  
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(abc)
  ],
  declarations: [AbcComponent]
})
export class HomeModuleModule { }
