
import { MyModule1Module } from './my-module1/my-module1.module';
import { MyModuleModule } from './my-module/my-module.module';
import { Test1Component } from './my-module/test1/test1.component';
import { Test11Component } from './my-module1/test11/test11.component';
import { Route, RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCheckboxModule } from '@angular/material';
import {MatSnackBarModule} from '@angular/material';
import {MatRadioModule} from '@angular/material';
import {MatButtonModule} from '@angular/material';
import {MatMenuModule} from '@angular/material';
import {MdMenuTrigger} from '@angular/material';//
import {MatSidenavModule} from '@angular/material';
import {MatButtonToggleModule} from '@angular/material';
import {MatChipsModule} from '@angular/material';
import {MatIconModule} from '@angular/material';
import {MatProgressSpinnerModule} from '@angular/material';
import {MatProgressBarModule} from '@angular/material';
import {MatPaginatorModule} from '@angular/material';
import {MatTooltipModule} from '@angular/material';
import {MatListModule} from '@angular/material';
import {MatGridListModule} from '@angular/material';
import { AppComponent } from './app.component';

import { NewCwpComponent } from './new-cwp/new-cwp.component';
import { NewCwp1Component } from './new-cwp1/new-cwp1.component';
import { TestComponent } from './home-module/test/test.component';


const appRoutes: Routes = 
[
  { path: 'new-cwp', component: NewCwpComponent },
  { path: 'new-cwp1',      component: NewCwp1Component },
  //{ path: 'my-module-test1', component : Test1Component },
  //{ path: 'my-module1-test11',component: Test11Component },
  {
    path: 'my-module-test1',
    loadChildren: 'app/my-module/my-module.module#MyModuleModule',
  },
  
  
];

@NgModule({
  declarations: [
    AppComponent,
    NewCwpComponent,
    NewCwp1Component,
    
],
  imports: [
    
      RouterModule.forRoot(
        appRoutes,{ enableTracing: true }),
      
        BrowserModule,

    BrowserModule,

    MatCheckboxModule,
    MatButtonModule,
    MatMenuModule,
    MatSidenavModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    MatRadioModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatPaginatorModule,
    MatTooltipModule,
    MatListModule,
    MatGridListModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

