import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCwpComponent } from './new-cwp.component';

describe('NewCwpComponent', () => {
  let component: NewCwpComponent;
  let fixture: ComponentFixture<NewCwpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCwpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCwpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
