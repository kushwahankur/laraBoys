import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-cwp',
  template: `
  <h2>new-cwp</h2>
  <p>Get your new-cwp here</p>

  <button routerLink="/sidekicks">Go to sidekicks</button>
`,
  templateUrl: './new-cwp.component.html',
  styleUrls: ['./new-cwp.component.scss']
})
export class NewCwpComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
