import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { XyzComponent } from './xyz/xyz.component';

const abc: Routes = [
  { path: '',  component: XyzComponent }
  
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(abc)
  ],
  declarations: [XyzComponent]
})
export class HouseModuleModule { }
