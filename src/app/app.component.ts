import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
  <h1>Angular Router</h1>
  <nav>
    <a routerLink="/new-cwp1" routerLinkActive="active">new-cwp1</a>
    <a routerLink="/new-cwp" routerLinkActive="active">new-cwp</a>
  </nav>
  <router-outlet></router-outlet>
`,
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
}
