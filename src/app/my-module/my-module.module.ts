import { Route,RouterModule,Routes,RoutesRecognized } from '@angular/router';
import { Test1Component } from '../home-module1/test1/test1.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const appRoutes1: Routes = [
  {
    path:'',
    component: Test1Component
    
  }
 
];



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(appRoutes1),
    RouterModule.forRoot(
      appRoutes1,
      { enableTracing: true }),
  ],
  declarations: [Test1Component]
})
export class MyModuleModule { }
