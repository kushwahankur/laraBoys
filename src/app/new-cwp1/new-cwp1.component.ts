import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-cwp1',
  template: `
  <h2>new-cwp1</h2>
  <p>Get your new-cwp1 here</p>`,
  templateUrl: './new-cwp1.component.html',
  styleUrls: ['./new-cwp1.component.scss']
})
export class NewCwp1Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
