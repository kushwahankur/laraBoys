import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCwp1Component } from './new-cwp1.component';

describe('NewCwp1Component', () => {
  let component: NewCwp1Component;
  let fixture: ComponentFixture<NewCwp1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCwp1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCwp1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
